import {
  signOut,
  updateProfile,
  setPersistence,
  onAuthStateChanged,
  browserLocalPersistence,
  createUserWithEmailAndPassword,
} from "firebase/auth";

import { auth } from "config";

export function getCurrentUser() {
  return auth.currentUser;
}

export function onAuthChanged(next) {
  return onAuthStateChanged(auth, next);
}

export async function logout() {
  try {
    await signOut(auth);
  } catch (error) {
    switch (error.code) {
      default:
        console.error(error);
        throw new Error("Hubo un error al intentar al cerrar la sesión");
    }
  }
}

export async function createUser({ name, email, password }) {
  try {
    await setPersistence(auth, browserLocalPersistence);
    const { user } = await createUserWithEmailAndPassword(
      auth,
      email,
      password
    );
    await updateProfile(auth.currentUser, {
      displayName: name,
    });
    console.log("service", auth.currentUser);
    return user;
  } catch (error) {
    switch (error.code) {
      case "auth/email-already-in-use":
        throw new Error(`Ya existe una cuenta con el correo ${email}`);
      default:
        console.error(error);
        throw new Error("Hubo un error al intentar crear el usuario");
    }
  }
}
