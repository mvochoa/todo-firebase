import React from "react";
import ReactDOM from "react-dom";

import Pages from "./pages";
import reportWebVitals from "./reportWebVitals";

import AlertProvider from "context/alertContext";

import "./index.css";
import "./config.js";

console.info(`Version: ${process.env.REACT_APP_VERSION}`);
ReactDOM.render(
  <React.StrictMode>
    <AlertProvider>
      <Pages />
    </AlertProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
