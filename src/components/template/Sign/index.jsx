import { Form, Formik } from "formik";

import Text from "components/atoms/Text";
import Box from "components/atoms/Box";
import Button from "components/molecules/Button";
import BaseTemplate from "components/template/Base";

import Logo from "assets/logo.svg";

export default function Sign({
  children,
  textSubmit,
  textRedirect,
  toRedirect,
  initialValues,
  onSubmit,
  onValidate = () => {},
}) {
  return (
    <BaseTemplate>
      <section className="w-full">
        <header className="text-center">
          <img
            src={Logo}
            className="h-20 my-2 mx-auto md:h-16"
            alt="React Logo"
          />
          <Text variant="title">TODO con Firebase</Text>
        </header>
        <Box className="w-10/12 my-8 mx-auto lg:w-4/12">
          <Formik
            onSubmit={onSubmit}
            validate={onValidate}
            initialValues={initialValues}
          >
            {(p) => (
              <Form>
                <main className="space-y-6">{children}</main>
                <footer className="text-center mt-8">
                  <Button
                    loading={p.isSubmitting}
                    disabled={!p.isValid}
                    type="submit"
                    className="w-full mb-4"
                  >
                    {textSubmit}
                  </Button>
                  <Button to={toRedirect} variant="text">
                    {textRedirect}
                  </Button>
                </footer>
              </Form>
            )}
          </Formik>
        </Box>
      </section>
    </BaseTemplate>
  );
}
