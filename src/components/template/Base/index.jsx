import classnames from "classnames";

export default function BaseTemplate({ children, className }) {
  return (
    <main
      className={classnames(
        "min-w-screen min-h-screen flex justify-center items-center",
        className
      )}
    >
      {children}
    </main>
  );
}
