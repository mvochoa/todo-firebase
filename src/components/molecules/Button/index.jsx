import { Link } from "react-router-dom";
import classnames from "classnames";

import ProgressIcon from "assets/icons/Progress";

export default function Button({
  variant,
  secondary,
  to,
  children,
  disabled = false,
  loading = false,
  ...rest
}) {
  const className = classnames(
    "text-sm font-bold leading-6 transition-all",
    {
      "px-6 py-3 rounded-lg": !variant,
      "text-white": !secondary || disabled,
      "text-primary": secondary || variant === "text",
      "bg-primary": !secondary && !variant && !disabled,
      "bg-secondary": secondary && !disabled,
      "bg-gray-300": disabled,
    },
    rest.className
  );

  if (to) {
    return (
      <Link {...rest} to={!disabled ? to : "#"} className={className}>
        {children}
      </Link>
    );
  }

  return (
    <button {...rest} disabled={disabled || loading} className={className}>
      {loading ? (
        <div className="flex items-center">
          <ProgressIcon className="animate-spin" />
          <span className="grow">Procesando...</span>
        </div>
      ) : (
        children
      )}
    </button>
  );
}
