import { useState, useEffect } from "react";
import { useField } from "formik";
import classnames from "classnames";

export default function Field({ type = "text", label, className, ...rest }) {
  const [field, meta] = useField({ type, ...rest });
  const [error, setError] = useState(null);

  useEffect(() => {
    if (meta.touched && meta.error) {
      setError(meta.error);
    } else {
      setError(null);
    }
  }, [meta]);

  return (
    <div className={classnames(className)}>
      <label className="block font-normal text-gray-600 text-sm mb-1">
        {label}
      </label>
      <input
        {...field}
        {...rest}
        type={type}
        className={classnames(
          "w-full text-primary-txt bg-white text-base font-normal font-sans block border rounded px-2.5 py-1.5 outline-none",
          {
            "border-red-600": error,
          }
        )}
      />
      {error && <p className="text-xs pt-2 text-red-600">{error}</p>}
    </div>
  );
}
