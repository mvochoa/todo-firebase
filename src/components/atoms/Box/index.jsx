import classnames from "classnames";

export default function Box({ className, ...rest }) {
  return (
    <div
      {...rest}
      className={classnames(
        "bg-white shadow rounded-md px-5 py-4 md:px-10 md:py-9",
        className
      )}
    />
  );
}
