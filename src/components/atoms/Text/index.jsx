import classnames from "classnames";

export default function Text({ variant = "", className, children }) {
  switch (variant.toLowerCase()) {
    case "title":
      return (
        <h1
          className={classnames(
            "text-3xl text-primary-txt font-sans font-extrabold",
            className
          )}
        >
          {children}
        </h1>
      );
    case "subtitle":
      return (
        <h2
          className={classnames(
            "text-2xl text-primary-txt font-sans font-bold",
            className
          )}
        >
          {children}
        </h2>
      );
    default:
      return (
        <p
          className={classnames(
            "text-base text-primary-txt font-sans font-normal",
            className
          )}
        >
          {children}
        </p>
      );
  }
}
