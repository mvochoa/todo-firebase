import { useEffect, useState } from "react";
import classnames from "classnames";

import CloseIcon from "assets/icons/Close";
import CheckCircleIcon from "assets/icons/CheckCircle";
import ExclamationIcon from "assets/icons/Exclamation";
import ExclamationCircleIcon from "assets/icons/ExclamationCircle";

import Button from "components/molecules/Button";

import useAlert, {
  ALERT_SUCCESS,
  ALERT_WARNING,
  ALERT_ERROR,
} from "hooks/useAlert";

export default function Alert({
  className,
  id,
  message,
  type = ALERT_SUCCESS,
}) {
  const { removeAlert } = useAlert();
  const [icon, setIcon] = useState(null);

  useEffect(() => {
    switch (type) {
      case ALERT_SUCCESS:
        setIcon(<CheckCircleIcon className="mr-2 text-green-500" />);
        break;
      case ALERT_WARNING:
        setIcon(<ExclamationIcon className="mr-2 text-yellow-500" />);
        break;
      case ALERT_ERROR:
        setIcon(<ExclamationCircleIcon className="mr-2 text-red-500" />);
        break;
      default:
        setIcon(null);
        break;
    }
  }, [type]);

  useEffect(() => {
    setTimeout(() => {
      removeAlert(id);
    }, 12000);
  }, [id, removeAlert]);

  return (
    <div
      className={classnames(
        "animate-fade-in-down flex items-center px-4 py-3 w-6/12 rounded opacity-80 leading-6",
        {
          "bg-green-100": type === ALERT_SUCCESS,
          "bg-yellow-100": type === ALERT_WARNING,
          "bg-red-100": type === ALERT_ERROR,
        },
        className
      )}
    >
      {icon}
      <span
        className={classnames("grow font-sans font-medium text-sm", {
          "text-green-800": type === ALERT_SUCCESS,
          "text-yellow-800": type === ALERT_WARNING,
          "text-red-800": type === ALERT_ERROR,
        })}
      >
        {message}
      </span>
      <Button
        variant="icon"
        className={classnames({
          "text-green-500": type === ALERT_SUCCESS,
          "text-yellow-500": type === ALERT_WARNING,
          "text-red-500": type === ALERT_ERROR,
        })}
        onClick={() => removeAlert(id)}
      >
        <CloseIcon />
      </Button>
    </div>
  );
}
