import useAlert from "hooks/useAlert";

import Alert from "components/organisms/Alert";

export default function BoxAlerts() {
  const { alerts } = useAlert();

  return (
    <div className="fixed inset-x-0 bottom-0 space-y-4 flex flex-col items-center mb-4">
      {alerts.map(({ id, message, type }, index) => (
        <Alert key={index} id={id} message={message} type={type} />
      ))}
    </div>
  );
}
