import { useState, useEffect } from "react";
import { Route } from "react-router-dom";

import useAuth from "hooks/useAuth";

export default function CustomRoute({ auth = false, ...rest }) {
  const [wait, setWait] = useState(auth);
  const { user } = useAuth();

  useEffect(() => {
    if (auth && user) {
      setWait(false);
      return;
    }

    setTimeout(() => {
      setWait(false);
    }, 2000);
  }, [auth, user]);

  if (wait) return null;
  return <Route {...rest} />;
}
