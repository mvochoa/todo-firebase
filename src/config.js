import { getAuth } from "firebase/auth";
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

export const firebaseApp = initializeApp({
  apiKey: "AIzaSyBU8dcNHM8jknt0hCKB5fTy3KWjqOQT5kE",
  authDomain: "todo-cab09.firebaseapp.com",
  projectId: "todo-cab09",
  storageBucket: "todo-cab09.appspot.com",
  messagingSenderId: "577546496183",
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  appId: "1:577546496183:web:c5b2b4acc0c79568370532",
});

export const database = getDatabase(firebaseApp);
export const auth = getAuth();
