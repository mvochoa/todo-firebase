import { createContext, useState } from "react";

export const ALERT_SUCCESS = "success";
export const ALERT_WARNING = "warning";
export const ALERT_ERROR = "error";

export const AlertContext = createContext();

export default function AlertProvider({ children }) {
  const [alerts, setAlerts] = useState([]);

  function createAlert({ message, type }) {
    setAlerts([
      ...alerts,
      {
        id: Math.random(),
        message,
        type,
      },
    ]);
  }

  function removeAlert(id) {
    setAlerts(alerts.filter((i) => i.id !== id));
  }

  return (
    <AlertContext.Provider value={{ alerts, createAlert, removeAlert }}>
      {children}
    </AlertContext.Provider>
  );
}
