import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import {
  createUser,
  logout,
  getCurrentUser,
  onAuthChanged,
} from "services/auth";

export default function useAuth() {
  const [user, setUser] = useState(getCurrentUser());
  const history = useHistory();

  useEffect(() => {
    const pathname = history.location.pathname;
    const paths = ["/sign-in", "/sign-up"];
    if (user && paths.includes(pathname)) {
      history.push("/");
      return;
    }

    if (!user && !paths.includes(pathname)) {
      history.push("/sign-in");
    }
  }, [user]);

  useEffect(() => {
    const unsubscribe = onAuthChanged((user) => setUser(user));

    return () => unsubscribe();
  }, []);

  return { user, createUser, logout };
}
