import { useContext } from "react";

import {
  AlertContext,
  ALERT_SUCCESS,
  ALERT_WARNING,
  ALERT_ERROR,
} from "context/alertContext";

export { ALERT_SUCCESS, ALERT_WARNING, ALERT_ERROR };

export default function useAlert() {
  return useContext(AlertContext);
}
