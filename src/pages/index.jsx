import { BrowserRouter, Switch } from "react-router-dom";

import Home from "pages/Home";
import SignUp from "pages/SignUp";
import SignIn from "pages/SignIn";
import NotFound from "pages/NotFound";

import Route from "components/organisms/Route";
import BoxAlerts from "components/organisms/BoxAlerts";

export default function Pages() {
  return (
    <BrowserRouter>
      <Switch>
        <Route auth exact path="/">
          <Home />
        </Route>
        <Route path="/sign-in">
          <SignIn />
        </Route>
        <Route path="/sign-up">
          <SignUp />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
      <BoxAlerts />
    </BrowserRouter>
  );
}
