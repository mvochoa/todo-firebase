import useAuth from "hooks/useAuth";

import Button from "components/molecules/Button";
import Text from "components/atoms/Text";
import BaseTemplate from "components/template/Base";

export default function Home() {
  const { user, logout } = useAuth();

  return (
    <BaseTemplate className="flex-col">
      <Text variant="title" className="mb-4 text-primary">
        Hola{" "}
        <Text variant="subtitle" className="mb-4">
          {user.displayName}
        </Text>
      </Text>

      <Button onClick={logout}>Cerrar sesión</Button>
    </BaseTemplate>
  );
}
