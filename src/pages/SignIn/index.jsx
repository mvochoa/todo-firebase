import { useHistory } from "react-router-dom";

import useAuth from "hooks/useAuth";
import useAlert, { ALERT_ERROR, ALERT_SUCCESS } from "hooks/useAlert";

import Field from "components/molecules/Field";
import SignTemplate from "components/template/Sign";

export default function SignIn() {
  const { login } = useAuth();
  const { createAlert } = useAlert();
  const history = useHistory();

  async function handleSubmit(values, actions) {
    try {
      await login(values);
      actions.resetForm();
      createAlert({
        message: "Inicio de sesión exitoso",
        type: ALERT_SUCCESS,
      });
      history.push("/");
    } catch (error) {
      createAlert({
        message: error.message,
        type: ALERT_ERROR,
      });
    } finally {
      actions.setSubmitting(false);
    }
  }

  return (
    <SignTemplate
      textSubmit="Iniciar sesion"
      textRedirect="Crear cuenta"
      toRedirect="/sign-up"
      onSubmit={handleSubmit}
      initialValues={{
        email: "",
        password: "",
      }}
    >
      <Field required name="email" type="email" label="Correo electronico" />
      <Field required name="password" type="password" label="Contraseña" />
    </SignTemplate>
  );
}
