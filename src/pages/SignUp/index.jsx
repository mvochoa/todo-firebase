import { useHistory } from "react-router-dom";

import useAuth from "hooks/useAuth";
import useAlert, { ALERT_ERROR, ALERT_SUCCESS } from "hooks/useAlert";

import Field from "components/molecules/Field";
import SignTemplate from "components/template/Sign";

export default function SignUp() {
  const { createUser } = useAuth();
  const { createAlert } = useAlert();
  const history = useHistory();

  async function handleSubmit(values, actions) {
    try {
      await createUser(values);
      actions.resetForm();
      createAlert({
        message: "Listo se ha registrado el usuario",
        type: ALERT_SUCCESS,
      });
      history.push("/");
    } catch (error) {
      createAlert({
        message: error.message,
        type: ALERT_ERROR,
      });
    } finally {
      actions.setSubmitting(false);
    }
  }

  function handleValidate(values) {
    const errors = {};

    if (values.password !== "") {
      if (
        values.confirm_password !== "" &&
        values.password !== values.confirm_password
      ) {
        errors.confirm_password = "Las contraseñas no coinciden";
      }

      if (values.password.length < 6) {
        errors.password = "La contraseña debe tener mas de 6 caracteres";
      }
    }

    return errors;
  }

  return (
    <SignTemplate
      textSubmit="Crear cuenta"
      textRedirect="Iniciar sesion"
      toRedirect="/sign-in"
      onSubmit={handleSubmit}
      onValidate={handleValidate}
      initialValues={{
        name: "",
        email: "",
        password: "",
        confirm_password: "",
      }}
    >
      <Field required name="name" label="Nombre completo" />
      <Field required name="email" type="email" label="Correo electronico" />
      <Field required name="password" type="password" label="Contraseña" />
      <Field
        required
        name="confirm_password"
        type="password"
        label="Confirmar contraseña"
      />
    </SignTemplate>
  );
}
