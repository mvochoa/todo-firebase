import Button from "components/molecules/Button";
import Text from "components/atoms/Text";
import BaseTemplate from "components/template/Base";

export default function NotFound() {
  return (
    <BaseTemplate>
      <div className="p-4 md:flex">
        <Text
          variant="title"
          className="!text-primary md:border-r md:pr-4 md:mr-4"
        >
          404
        </Text>
        <div>
          <Text variant="subtitle">Pagina no encontrada</Text>
          <Text className="!text-secondary-txt pt-1">
            Por favor revise la URL en la barra de dirección e intente de nuevo
          </Text>
          <div className="mt-8">
            <Button to="/">Pagina de inicio</Button>
          </div>
        </div>
      </div>
    </BaseTemplate>
  );
}
